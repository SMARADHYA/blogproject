import { Body, Controller, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { User } from './User.schema';
import { UserCreateDto } from './UserCreate.dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {

  constructor(private userService: UsersService){}

  @Post()
  @UsePipes(ValidationPipe)
 async createUser(@Body() userCreateDto: UserCreateDto):Promise<User>{
    let newUser = await this.userService.create(userCreateDto);
    console.log(newUser);
   return newUser;
  }
}
