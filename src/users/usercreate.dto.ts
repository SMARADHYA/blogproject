import { IsEmpty, IsNotEmpty } from 'class-validator';
export class UserCreateDto {
  @IsNotEmpty()
  firstName:string
  @IsNotEmpty()
  lastName:string
  @IsNotEmpty()
  email:string
  city:string
  comments:Array<Comment>
}