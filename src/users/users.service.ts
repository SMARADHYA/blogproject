import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import {v1 as uuid} from 'uuid'
import { UserCreateDto } from './UserCreate.dto';
import { Messages } from '../Messages.data';
import { Model } from "mongoose";
import { User, UserDocument } from "./User.schema"

@Injectable()
export class UsersService {

  constructor(@InjectModel(User.name) private userModel : Model<UserDocument>){}

  async create(creteUserDTO:UserCreateDto):Promise<User>{

    let newUser = new this.userModel(creteUserDTO);
    return await newUser.save();

  }
  async findAll():Promise<User[]>{

    return await this.userModel.find();

  }

}
