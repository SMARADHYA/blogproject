import { Body, Controller, Get, Post } from '@nestjs/common';
import { Blog } from 'src/blogs/Blog.schema';
import { User } from 'src/users/User.schema';
import { CommentCreateDTO } from './CommentCreate.dto';
import { CommentsService } from './comments.service';
import { BlogsService } from 'src/blogs/blogs.service';

@Controller('comments')
export class CommentsController {
  constructor(private commentService: CommentsService,private blogService: BlogsService) { }

    @Post()
    async create(@Body() commentCreateDto: CommentCreateDTO,author: User) {
        let ret = await this.commentService.create(commentCreateDto,author);
        let data = await this.blogService.findOne(ret.blog);
        data.comment.push(ret['_id'])
        let update = await this.blogService.update(data);
        console.log(update);
        
        console.log(data);
        console.log(ret);
        console.log(ret['blog'])
        return ret
    }
    @Get()
    async getAll() {
        return this.commentService.findAll();
    }
}
