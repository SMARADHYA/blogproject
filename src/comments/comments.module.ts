
import { Module } from '@nestjs/common';
import { CommentsController } from './comments.controller';
import { CommentsService } from './comments.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CommentSchema ,Comment} from './Comment.schema';
import { BlogsModule } from 'src/blogs/blogs.module';
@Module({
  imports: [MongooseModule.forFeature([
    {name:Comment.name,schema:CommentSchema},
  ]),BlogsModule],
  controllers: [CommentsController],
  providers: [CommentsService],
  exports: [CommentsService],
})
export class CommentsModule {}
