import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CommentCreateDTO } from "./CommentCreate.dto";
import { Comment, CommentDocument } from "./Comment.schema";
import { User, UserDocument } from "../users/User.schema"
import { Blog } from "src/blogs/Blog.schema";

@Injectable()
export class CommentsService {
  constructor(@InjectModel(Comment.name) private commentModel: Model<CommentDocument>) {
  }

  create(commentData: CommentCreateDTO, author: User) {
    const createdComment = new this.commentModel({
      ...commentData,
      author,
    });
    return createdComment.save();
  }

  async findAll() {
      return await this.commentModel.find().populate('blog');
  }
  
}
