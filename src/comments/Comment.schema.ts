import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { User } from "../users/User.schema";
import { Blog } from "../blogs/Blog.schema";
import { Transform, Type } from 'class-transformer';
import * as mongoose from "mongoose";

export type CommentDocument = Comment & Document;

@Schema()
export class Comment {
  @Transform(({ value }) => value.toString())
  _id: string;


  @Prop()
  title: string;
  @Prop()
  body: string;
  @Prop()
  email: string;
  @Prop({type:mongoose.Schema.Types.ObjectId,ref: User.name})
  author:User;
  @Prop({type:mongoose.Schema.Types.ObjectId,ref:'Blog'})
  @Type(() => Blog)
  blog:any;
}
export const CommentSchema = SchemaFactory.createForClass(Comment)