export class CommentCreateDTO {
  id : string
  name : string
  body : string
  email: string
  author : string
  blog : string
}