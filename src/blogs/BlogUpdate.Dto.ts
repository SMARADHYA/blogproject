export interface BlogUpdateDto {
  id: string
  comment: Array<string>
}