export class BlogCreateDTO {
  id: string
  title:string
  body:string
  author:string
  comments:Array<string>
}