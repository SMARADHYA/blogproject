import { Module } from '@nestjs/common';
import { BlogsController } from './blogs.controller';
import { BlogsService } from './blogs.service';
import { MongooseModule } from '@nestjs/mongoose';
import { BlogSchema ,Blog} from './Blog.schema';



@Module({
  imports: [MongooseModule.forFeature([
    {name:Blog.name,schema:BlogSchema},
  ])],
  controllers: [BlogsController],
  providers: [BlogsService],
  exports: [BlogsService],
})
export class BlogsModule {}
