import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId } from 'mongoose';
import * as mongoose from 'mongoose';
import { User } from '../users/user.schema';
import { Transform, Type } from 'class-transformer';
import { Comment } from '../comments/Comment.schema';
 
export type BlogDocument = Blog & Document;
 
@Schema()
export class Blog {
  @Transform(({ value }) => value.toString())
  _id: ObjectId;
 
  @Prop()
  title: string;
 
  @Prop()
  body: string;
 
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  @Type(() => User)
  author: User;

  @Prop({ type: [mongoose.Schema.Types.ObjectId], ref: Comment.name })
  @Type(() => Comment)
  comment:any;
  // @Prop()
  // comment:Array<string>;
}
 
export const BlogSchema = SchemaFactory.createForClass(Blog);