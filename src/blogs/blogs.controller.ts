import { Body, Controller, Get, Post } from "@nestjs/common";
import { BlogCreateDTO } from "./BlogCreate.dto";
import {  Blog} from "./Blog.schema";
import { BlogsService } from "./blogs.service";
import { User, UserDocument } from "../users/User.schema"



@Controller('blogs')
export class BlogsController {

  constructor(private blogService: BlogsService) { }

    @Post()
    async create(@Body() blogCreateDto: BlogCreateDTO,author: User): Promise<Blog> {
        let ret = await this.blogService.create(blogCreateDto,author);
        console.log(ret)
        return ret
    }
    @Get()
    async getAll() {
        return this.blogService.findAll();
    }


}
