import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { BlogCreateDTO } from "./BlogCreate.dto";
import { Blog, BlogDocument } from "./Blog.schema";
import { User, UserDocument } from "../users/User.schema"
import { BlogUpdateDto } from "./BlogUpdate.dto";


@Injectable()
export class BlogsService {
  constructor(@InjectModel(Blog.name) private blogModel: Model<BlogDocument>) {
  }

  create(blogData: BlogCreateDTO, author: User) {
    const createdPost = new this.blogModel({
      ...blogData,
      author,
    });
    return createdPost.save();
  }

  async findAll() {
      return await this.blogModel.find().populate('author').populate('comment');
  }
  async findOne(id: string): Promise<Blog> {
    return await this.blogModel.findOne({ _id: id })
}
async update(blog: any): Promise<Blog> {

  return await this.blogModel.findOneAndUpdate({ _id: blog._id },
      { comment: blog.comment }, {
      new: true
  })

}
}
