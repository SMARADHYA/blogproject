import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';
import { MONGO_CONNECTION} from './app.properties';
import { BlogsModule } from './blogs/blogs.module';
import { CommentsModule } from './comments/comments.module';

@Module({
  imports: [UsersModule,BlogsModule,CommentsModule,MongooseModule.forRoot(MONGO_CONNECTION)]
 
})
export class AppModule {}
